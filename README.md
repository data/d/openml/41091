# OpenML dataset: spellman_yeast

https://www.openml.org/d/41091

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Two colour spotted cDNA array data set of a series of experiments to identify which genes in Yeast are cell cycle regulated.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41091) of an [OpenML dataset](https://www.openml.org/d/41091). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41091/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41091/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41091/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

